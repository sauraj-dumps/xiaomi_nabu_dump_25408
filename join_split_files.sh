#!/bin/bash

cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat product/data-app/MiuiScanner/MiuiScanner.apk.* 2>/dev/null >> product/data-app/MiuiScanner/MiuiScanner.apk
rm -f product/data-app/MiuiScanner/MiuiScanner.apk.* 2>/dev/null
cat product/data-app/MiShop/MiShop.apk.* 2>/dev/null >> product/data-app/MiShop/MiShop.apk
rm -f product/data-app/MiShop/MiShop.apk.* 2>/dev/null
cat product/data-app/MIUINotes/MIUINotes.apk.* 2>/dev/null >> product/data-app/MIUINotes/MIUINotes.apk
rm -f product/data-app/MIUINotes/MIUINotes.apk.* 2>/dev/null
cat product/data-app/MIUIYoupin/MIUIYoupin.apk.* 2>/dev/null >> product/data-app/MIUIYoupin/MIUIYoupin.apk
rm -f product/data-app/MIUIYoupin/MIUIYoupin.apk.* 2>/dev/null
cat product/data-app/MIMediaEditor/MIMediaEditor.apk.* 2>/dev/null >> product/data-app/MIMediaEditor/MIMediaEditor.apk
rm -f product/data-app/MIMediaEditor/MIMediaEditor.apk.* 2>/dev/null
cat product/data-app/SmartHome/SmartHome.apk.* 2>/dev/null >> product/data-app/SmartHome/SmartHome.apk
rm -f product/data-app/SmartHome/SmartHome.apk.* 2>/dev/null
cat product/data-app/MIpayPad_NO_NFC/MIpayPad_NO_NFC.apk.* 2>/dev/null >> product/data-app/MIpayPad_NO_NFC/MIpayPad_NO_NFC.apk
rm -f product/data-app/MIpayPad_NO_NFC/MIpayPad_NO_NFC.apk.* 2>/dev/null
cat product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null >> product/priv-app/MiuiCamera/MiuiCamera.apk
rm -f product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null
cat product/priv-app/MiuiExtraPhoto/MiuiExtraPhoto.apk.* 2>/dev/null >> product/priv-app/MiuiExtraPhoto/MiuiExtraPhoto.apk
rm -f product/priv-app/MiuiExtraPhoto/MiuiExtraPhoto.apk.* 2>/dev/null
cat product/priv-app/MIUIMusicPAD/MIUIMusicPAD.apk.* 2>/dev/null >> product/priv-app/MIUIMusicPAD/MIUIMusicPAD.apk
rm -f product/priv-app/MIUIMusicPAD/MIUIMusicPAD.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/priv-app/MIUIVideoPad/MIUIVideoPad.apk.* 2>/dev/null >> product/priv-app/MIUIVideoPad/MIUIVideoPad.apk
rm -f product/priv-app/MIUIVideoPad/MIUIVideoPad.apk.* 2>/dev/null
cat product/priv-app/MIUIBrowserPad/MIUIBrowserPad.apk.* 2>/dev/null >> product/priv-app/MIUIBrowserPad/MIUIBrowserPad.apk
rm -f product/priv-app/MIUIBrowserPad/MIUIBrowserPad.apk.* 2>/dev/null
cat product/priv-app/MIUIGallery/MIUIGallery.apk.* 2>/dev/null >> product/priv-app/MIUIGallery/MIUIGallery.apk
rm -f product/priv-app/MIUIGallery/MIUIGallery.apk.* 2>/dev/null
cat product/app/VoiceAssistAndroidT/VoiceAssistAndroidT.apk.* 2>/dev/null >> product/app/VoiceAssistAndroidT/VoiceAssistAndroidT.apk
rm -f product/app/VoiceAssistAndroidT/VoiceAssistAndroidT.apk.* 2>/dev/null
cat product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null >> product/app/WebViewGoogle64/WebViewGoogle64.apk
rm -f product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null
cat product/app/HybridPlatform/HybridPlatform.apk.* 2>/dev/null >> product/app/HybridPlatform/HybridPlatform.apk
rm -f product/app/HybridPlatform/HybridPlatform.apk.* 2>/dev/null
cat product/app/VoiceTrigger/VoiceTrigger.apk.* 2>/dev/null >> product/app/VoiceTrigger/VoiceTrigger.apk
rm -f product/app/VoiceTrigger/VoiceTrigger.apk.* 2>/dev/null
cat product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null >> product/app/TrichromeLibrary64/TrichromeLibrary64.apk
rm -f product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null
cat product/app/MIUIAICR/MIUIAICR.apk.* 2>/dev/null >> product/app/MIUIAICR/MIUIAICR.apk
rm -f product/app/MIUIAICR/MIUIAICR.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk.* 2>/dev/null >> system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk
rm -f system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat vendor/lib64/librelight_only.so.* 2>/dev/null >> vendor/lib64/librelight_only.so
rm -f vendor/lib64/librelight_only.so.* 2>/dev/null
cat vendor_bootimg/05_dtbdump_?>qd^^".dtb.* 2>/dev/null >> vendor_bootimg/05_dtbdump_?>qd^^".dtb
rm -f vendor_bootimg/05_dtbdump_?>qd^^".dtb.* 2>/dev/null
